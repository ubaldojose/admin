@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="row">.</div>
            <form method="post" action="{{url('users')}}">
                {{csrf_field()}}
                @csrf
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Nuevo usuario</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-3">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Nombre">

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-xs-3">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Correo">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-xs-3">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Contraseña">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-xs-3">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Repita contraseña">
                            </div>
                        </div>
                        <br>
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Permisos</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <h4>Dependencias</h4>
                                        <label>
                                            <input name="ndependencias" type="checkbox" class="flat-red" value="0"> Navegar por las dependencias
                                        </label>
                                        <label>
                                            <input name="cdependencias" type="checkbox" class="flat-red" value="0"> Crear dependencias
                                        </label>
                                        <label>
                                            <input name="edependencias" type="checkbox" class="flat-red" value="0"> Editar  dependencias
                                        </label>
                                        <label>
                                            <input name="ddependencias" type="checkbox" class="flat-red" value="0"> Eliminar dependencias
                                        </label>
                                    </div>
                                    <div class="col-xs-3">
                                        <h4>Series</h4>
                                        <label>
                                            <input name="nseries" type="checkbox" class="flat-red" value="0"> Navegar por las Series
                                        </label>
                                        <label>
                                            <input name="cseries" type="checkbox" class="flat-red" value="0">Permiso para crear Series
                                        </label>
                                        <label>
                                            <input name="eseries" type="checkbox" class="flat-red" value="0">Permiso para Editar  Series
                                        </label>
                                        <label>
                                            <input name="dseries" type="checkbox" class="flat-red" value="0">Permiso para Eliminar Series
                                        </label>
                                    </div>
                                    <div class="col-xs-3">
                                        <h4>Subseries</h4>
                                        <label>
                                            <input name="nsubseries" type="checkbox" class="flat-red" value="0"> Navegar por las Subseries
                                        </label>
                                        <label>
                                            <input name="csubseries" type="checkbox" class="flat-red" value="0">Permiso para Crear Subseries
                                        </label>
                                        <label>
                                            <input name="esubseries" type="checkbox" class="flat-red" value="0">Permiso para Editar  Subseries
                                        </label>
                                        <label>
                                            <input name="dsubseries" type="checkbox" class="flat-red" value="0">Permiso para Eliminar Subseries
                                        </label>
                                    </div>
                                    <div class="col-xs-3">
                                        <h4>Ususarios</h4>
                                        <label>
                                            <input  name="nusuarios" type="checkbox" class="flat-red" value="0"> Navegar por los Usuarios
                                        </label>
                                        <label>
                                            <input  name="cusuarios" type="checkbox" class="flat-red" value="0">Permiso para Crear Usuarios
                                        </label>
                                        <label>
                                            <input  name="eusuarios" type="checkbox" class="flat-red" value="0">Permiso para Editar  Usuarios
                                        </label>
                                        <label>
                                            <input  name="dusuarios" type="checkbox" class="flat-red" value="0">Permiso para Eliminar Usuarios
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <button class="btn btn-success  col-md-8 col-md-offset-2">Guardar</button>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </form>
        </div>
    </div>
@endsection
